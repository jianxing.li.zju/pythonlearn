print('hello, world')
print('The quick brown fox', 'jumps over', 'the lazy dog')
print(300)
print(100 + 200)
print('100 + 200 =', 100 + 200)

#name = input('input your name: ')
#print(name)

print('1024 * 768 =', 1024*768)
print('I\'m \"OK\".')

print('\\ \t \n \\')
print(r'\\ \t \n')

print(r'''berry
cherry
larry \t \n
china''')

print('''berry
cherry
larry \t \n
china''')


n = 123
f = 456.789
s1 = 'Hello, world'
s2 = 'Hello, \'Adam\''
s3 = r'Hello, "Bart"'
s4 = r'''Hello,
Lisa!'''

print(n, f,  s1,  s2, s3, s4)

print(n, f, '\n', s1, '\n', s2, '\n', s3, '\n', s4)

